import java.util.Observable;

/**
 * Created by jan_w on 18.09.2017.
 */
public class NewsStation extends Observable {

    public NewsStation() {
            Watcher w = new Watcher();
            addObserver(w);
           // dodajOgladajacego();
    }

//    public void dodajOgladajacego(){
//        Watcher w = new Watcher();
//        addObserver(w);
//    }

    public void handleNews(String tekst, Integer level){
        setChanged();
        notifyObservers(new Message(tekst,level));
    }
}

/**
 * Created by jan_w on 18.09.2017.
 */
public class Message {

    private String text;
    private int level;

    public Message(String text, int level) {
        this.text = text;
        this.level = level;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}

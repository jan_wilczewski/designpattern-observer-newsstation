import java.util.Observable;
import java.util.Observer;

/**
 * Created by jan_w on 18.09.2017.
 */
public class Watcher implements Observer{

    private int progAkceptacji = 5;

    @Override
    public void update(Observable o, Object arg) {

        if (arg instanceof Message){
            Message message = (Message) arg;

            if (progAkceptacji < message.getLevel()){
                System.out.println("Watcher panikuje");
            }
            else {
                readNews(message.getText());
            }
        }
    }

    public void readNews(String message){
        System.out.println(message);
    }
}
